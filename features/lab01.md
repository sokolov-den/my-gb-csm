Metrics	                          Chef                Puppet	           Ansible	             Saltstack
Availability	                   ✔	               ✔	               ✔	                  ✔
Ease of Setup	              Not very easy	        Not very easy	         Easy	            Not very easy
Management	                  Not very easy	        Not very easy	         Easy               	Easy
Scalability	                 Highly Scalable	   Highly Scalable	    Highly Scalable	       Highly Scalable
Configuration language	       DSL(Ruby)	        DSL(PuppetDSL)	      YAML(Python)	         YAML(Python)
Interoperability	              High	                High	              High	                 High
